package com.example.cp.ui.routes;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cp.R;
import com.example.cp.firstNotes;
import com.example.cp.ui.Firstmarkers;
import com.example.cp.ui.home.HomeFragment;
import com.example.cp.ui.home.Routes;

import java.util.ArrayList;

public class RoutesFragment extends Fragment {

    private RoutesViewModel routesViewModel;
    ArrayList<firstNotes> notes = new ArrayList<firstNotes>();
    MarkerAdapter adapter2;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        routesViewModel =
                new ViewModelProvider(this).get(RoutesViewModel.class);

        View root = inflater.inflate(R.layout.fragment_routes, container, false);
        setInitialData();

        FragmentActivity c = getActivity();
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.NotesforRoute);
        RecyclerView recyclerView2 = (RecyclerView) root.findViewById(R.id.listofMarkers);

        NotesAdapter adapter = new NotesAdapter(c
                , notes);
         adapter2= new MarkerAdapter(c, Routes.marker,new MarkerAdapter.SelectMarkerListener() {
            @Override
            public void onMarkerSelect(String i) {

                Toast.makeText(getActivity(), "Зажмите, чтобы удалить", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMarkerLongClick(String a) {
                int i= Integer.parseInt((a.charAt(0)+""));
                if (Routes.marker.size()<i) {
                    Routes.marker.remove( i);
                    HomeFragment.i=i;
                    adapter2.set();
                }
            }}
        );

        // устанавливаем для списка адаптер
        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter2);

        return root;
    }
    private void setInitialData(){

        notes.add(new firstNotes("Новый маршрут", "Здесь будет ваше описание", 1));


    }
}