package com.example.cp.retrofitRoutes;

import android.util.Log;

import com.example.cp.ui.home.Routes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilderRoutes {


    public RetrofitBuilderRoutes(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = new OkHttpClient();

       // JsonParser parser = new JsonParser();
        //JsonObject jsonObj = parser.parse(jsonData).getAsJsonObject();
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://my-json-server.typicode.com/Korepina-J/cp/")//our backend
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

        RoutesApi routesApi = retrofit.create(RoutesApi.class);


        Call<String> getMarkerS = routesApi.GetMarkerS();
        /*Call<Route> getMarkers = routesApi.GetMarkers();
        getMarkers.enqueue(new Callback<Route>() {
            @Override
            public void onResponse(Call<Route> call, Response<Route> response) {

                if (response.isSuccessful()) {
                    Log.d("logs", "response " + response.body().toString());
                } else {
                    Log.d("logs", "response code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Route> call, Throwable t) {

                Log.d("logs", "failure " + t);
            }
        });*/
        getMarkerS.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> getMarkerS, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("logs", "response " + response.body().toString());

                   Route r = gson.fromJson(response.body().toString(),Route.class);
                    Routes.markers=r.getPlaces();
                } else {
                    Log.d("logs", "response code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<String> getMarkers, Throwable t) {

                Log.d("logs", "failure " + t);
            }
        });
        Call<Route> call = routesApi.PostRoutes();
        call.enqueue(new Callback<Route>() {
            @Override
            public void onResponse(Call<Route> call,Response<Route> response) {
                if (response.isSuccessful()) {



                    response.body().setPlaces(Routes.markers);

                } else {
                    // error response, no access to resource?
                }
            }

            @Override
            public void onFailure(Call<Route> call,Throwable t) {
            }
        });
    }
}

