package com.example.cp.retrofitUserData;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilderUserData {
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://github.com/NiceManItIsAboutMe/WEB/blob/master/public/testJSON")//input our site
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    LoginApi loginApi = retrofit.create(LoginApi.class);

    Call<UserData> getLoginCheck = loginApi.GetLoginCheck();

        public void onResponse(Call<UserData> call, Response<UserData> response) {
            if (response.isSuccessful()) {
                Log.d("logs","response " + response.body());
            } else {
                Log.d("logs","response code " + response.code());
            }
        }

        public void onFailure(Call<UserData> call, Throwable t) {
            Log.d("logs","failure " + t);
        }
}
