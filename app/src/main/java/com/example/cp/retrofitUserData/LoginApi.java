package com.example.cp.retrofitUserData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface LoginApi {
        @POST("PostLoginCheck")
        void PostLoginCheck (UserData userdata);
        @GET ("GetLoginCheck")
        Call<UserData> GetLoginCheck();
}
