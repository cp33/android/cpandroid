package com.example.cp.retrofitRoutes;

import android.view.ViewGroup;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RoutesApi {
    @Headers({
            "Content-type:application/json"
    })
    @GET("marker.json")
    Call <String> GetMarkerS();
    @POST("PostRoutes")
    Call <Route> PostRoutes();
}
