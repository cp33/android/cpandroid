package com.example.cp.retrofitRoutes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Route {

   @SerializedName("places")
   @Expose
   private ArrayList<Marker> places = null;

   public ArrayList<Marker> getPlaces() {
      return places;
   }

   public void setPlaces(ArrayList<Marker> places) {
      this.places = places;
   }

}
