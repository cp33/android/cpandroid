package com.example.cp.ui.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Markers {
    int id;

    public String name;

    public String description;
    double price;

    public Double lat;

    public Double lng;
public Markers (int i,String n,String d,double lat1, double lng1)
{
    id=i;
    name=n;
    description=d;
    lat=lat1;
    lng=lng1;
}
}
