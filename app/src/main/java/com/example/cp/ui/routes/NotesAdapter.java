package com.example.cp.ui.routes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cp.R;
import com.example.cp.firstNotes;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    private final LayoutInflater inflater;
    private final List<firstNotes> notes;

    public NotesAdapter(Context context,List<firstNotes> notes)
    {
        this.notes=notes;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public NotesAdapter.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_note_for_route, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotesAdapter.ViewHolder holder, int position) {
       firstNotes note=notes.get(position);
        holder.nameView.setText(note.name);
        holder.descriptionView.setText(note.description);
        holder.sumView.setText(Integer.toString(note.sum));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView nameView, descriptionView,sumView;
        ViewHolder(View view){
            super(view);

            nameView = (TextView) view.findViewById(R.id.name);
            descriptionView = (TextView) view.findViewById(R.id.description);
            sumView = (TextView) view.findViewById(R.id.sum);
        }
}
}
