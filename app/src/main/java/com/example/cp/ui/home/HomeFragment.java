package com.example.cp.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.cp.R;
import com.example.cp.retrofitRoutes.RetrofitBuilderRoutes;
import com.example.cp.ui.markers.MarkersViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements OnMapReadyCallback {

    private HomeViewModel homeViewModel;
    private GoogleMap mMap;
    private View infoWindow;
    private Marker curMarker;
    private Button delMarker;
    public static int i=-1;

    @Override
    public void onResume() {
        super.onResume();
if (i>0)
        clear(this.i);
    }
    public  void clear(int i)
    {
        Routes.marker.remove( i);
        mMap.clear();
        for(i=0;i<Routes.marker.size();i++)
        {
            LatLng ll=new LatLng(Routes.marker.get(i).lat,Routes.marker.get(i).lng);
            MarkerOptions markerOptions=new MarkerOptions();
            markerOptions.position(ll);
            markerOptions.title(Routes.marker.get(i).name);
            markerOptions.snippet(Routes.marker.get(i).description);
            markerOptions.draggable(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
            mMap.addMarker(markerOptions);
        }
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
       //initialData();
       RetrofitBuilderRoutes retrofit=new RetrofitBuilderRoutes();
        Log.d("logs", "response code  home" );
      //  delMarker=(Button) root.findViewById(R.id.deleteMerker);
        infoWindow=inflater.inflate(R.layout.cust_info_window, null);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(Routes.marker.size()>0)
        {
            for(int i=0;i<Routes.marker.size();i++)
            {
                Log.d("logs", "HHH"+i );
                LatLng ll=new LatLng(Routes.marker.get(i).lat,Routes.marker.get(i).lng);
                MarkerOptions markerOptions=new MarkerOptions();
                markerOptions.position(ll);
                markerOptions.title(Routes.marker.get(i).name);
                markerOptions.snippet(Routes.marker.get(i).description);
                markerOptions.draggable(true);
               // mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
                mMap.addMarker(markerOptions);
            }

        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng)
            {
                MarkerOptions markerOptions=new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Новый пункт");
                markerOptions.snippet("");
                markerOptions.draggable(true);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.addMarker(markerOptions);
                Routes.marker.add(new Markers(Routes.marker.size()+1, "Новый пункт","",latLng.latitude, latLng.longitude));
            }
        });

      /*  mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                curMarker=marker;
                return infoWindow;
            }
        });
     /*   mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker)
            {
                marker.showInfoWindow();

                return true;
            }
        });*/
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                MarkersViewModel.n=marker.getTitle();
                MarkersViewModel.d= marker.getSnippet();
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.nav_markers, null);
            }
        });
        mMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(@NonNull @NotNull Marker marker) {
                int i=0;
                while(Routes.marker.get(i).lat!=marker.getPosition().latitude&&Routes.marker.get(i).lng!=marker.getPosition().longitude)
                {
                    i++;
                }
                Routes.marker.remove( i);
                mMap.clear();
                for(i=0;i<Routes.marker.size();i++)
                {
                    LatLng ll=new LatLng(Routes.marker.get(i).lat,Routes.marker.get(i).lng);
                    MarkerOptions markerOptions=new MarkerOptions();
                    markerOptions.position(ll);
                    markerOptions.title(Routes.marker.get(i).name);
                    markerOptions.snippet(Routes.marker.get(i).description);
                    markerOptions.draggable(true);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
                    mMap.addMarker(markerOptions);
                }
            }
        });

    }
}