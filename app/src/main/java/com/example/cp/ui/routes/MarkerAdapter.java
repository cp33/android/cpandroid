package com.example.cp.ui.routes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cp.R;
import com.example.cp.ui.Firstmarkers;
import com.example.cp.ui.home.Markers;
import com.example.cp.ui.home.Routes;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class MarkerAdapter extends RecyclerView.Adapter<MarkerAdapter.ViewHolder> {
    private final LayoutInflater inflater;
   private  ArrayList<Markers> markers;
   private final SelectMarkerListener selectMarkerListener;
    public MarkerAdapter(Context context, ArrayList<Markers> markers,SelectMarkerListener selectMarkerListener)
    {
       this.markers=markers;
        this.inflater = LayoutInflater.from(context);
        this.selectMarkerListener = selectMarkerListener;
    }
    interface SelectMarkerListener {
        void onMarkerSelect(String id);

        void onMarkerLongClick(String id);
    }

    @Override
    public MarkerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_list_markers, parent, false);
        return new MarkerAdapter.ViewHolder(view,selectMarkerListener);
    }

    @Override
    public void onBindViewHolder(MarkerAdapter.ViewHolder holder, int position) {
        Markers marker= markers.get(position);
        holder.nameView.setText(marker.name);
    }

    @Override
    public int getItemCount() {
        return Routes.marker.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView nameView;
        private final SelectMarkerListener selectMarkerListener;
        ViewHolder(View view,SelectMarkerListener selectMarkerListener){
            super(view);
            this.selectMarkerListener = selectMarkerListener;
            nameView = (TextView) view.findViewById(R.id.MarkerName);

        }
        public void bind(final String id) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectMarkerListener.onMarkerSelect(id);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    selectMarkerListener.onMarkerLongClick(id);
                    return true;
                }
            });
    }

    }
    public void set()
    {
        markers=Routes.marker;
        notifyDataSetChanged();
    }
}
